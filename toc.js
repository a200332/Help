﻿[{
        "title": "HCEmrView",
        "key": "HCEmrView",
        "href": "HCEmrView.html",
        "activate": true,
        "children": [{
                "title": "属性",
                "key": "HCEmrView_Property",
                "href": "HCEmrView_Property.html",
                "children": [{
                        "title": "属性(字段)",
                        "key": "HCEmrView_Property",
                        "href": "HCEmrView_Property.html"
                    }
, {
                        "title": "属性(事件)",
                        "key": "HCEmrView_Property",
                        "href": "HCEmrView_Property.html"
                    }
]
                    }
, {
                "title": "方法",
                "key": "HCEmrView_Method",
                "href": "HCEmrView_Method.html",
                "children": [{
                        "title": "CreateEmrStyleItem 创建指定样式的Item",
                        "key": "HCEmrView.CreateEmrStyleItem",
                        "href": "HCEmrView.CreateEmrStyleItem.html"
                    }
, {
                        "title": "GetDataDeGroupText 获取指定数据组中的文本内容",
                        "key": "HCEmrView.GetDataDeGroupText",
                        "href": "HCEmrView.GetDataDeGroupText.html"
                    }
, {
                        "title": "GetDataForwardDeGroupText 从当前数据组起始位置往前找相同数据组的内容",
                        "key": "HCEmrView.GetDataForwardDeGroupText",
                        "href": "HCEmrView.GetDataForwardDeGroupText.html"
                    }
, {
                        "title": "InsertDeGroup 插入数据组",
                        "key": "HCEmrView.InsertDeGroup",
                        "href": "HCEmrView.InsertDeGroup.html"
                    }
, {
                        "title": "InsertDeItem 插入数据元",
                        "key": "HCEmrView.InsertDeItem",
                        "href": "HCEmrView.InsertDeItem.html"
                    }
, {
                        "title": "LoadFromStream 从二进制流加载文件",
                        "key": "HCEmrView.LoadFromStream",
                        "href": "HCEmrView.LoadFromStream.html"
                    }
, {
                        "title": "NewDeItem 新建数据元",
                        "key": "HCEmrView.NewDeItem",
                        "href": "HCEmrView.NewDeItem.html"
                    }
, {
                        "title": "SetActiveItemExtra 直接设置当前数据元的值为扩展内容",
                        "key": "HCEmrView.SetActiveItemExtra",
                        "href": "HCEmrView.SetActiveItemExtra.html"
                    }
, {
                        "title": "SetDeGroupText 设置数据组的内容为指定的文本",
                        "key": "HCEmrView.SetDeGroupText",
                        "href": "HCEmrView.SetDeGroupText.html"
                    }
, {
                        "title": "TraverseItem 遍历Item",
                        "key": "HCEmrView.TraverseItem",
                        "href": "HCEmrView.TraverseItem.html"
                    }
]
                    }
, {
                "title": "应用示例",
                "key": "HCEmrView_Sample",
                "href": "HCEmrView_Sample.html",
                "children": [{
                        "title": "保存文件",
                        "key": "HCEmrViewSample.保存文件",
                        "href": "HCEmrViewSample.保存文件.html"
                    }
, {
                        "title": "导出文件",
                        "key": "HCEmrViewSample.导出文件",
                        "href": "HCEmrViewSample.导出文件.html"
                    }
, {
                        "title": "加载文件",
                        "key": "HCEmrViewSample.加载文件",
                        "href": "HCEmrViewSample.加载文件.html"
                    }
, {
                        "title": "导入文件",
                        "key": "HCEmrViewSample.导入文件",
                        "href": "HCEmrViewSample.导入文件.html"
                    }
, {
                        "title": "结构化病历",
                        "key": "HCEmrViewSample.结构化病历",
                        "href": "HCEmrViewSample.结构化病历.html"
                    }
]
                    }
]
    }
 ,
    {
        "title": "HCView",
        "key": "HCView",
        "href": "HCView.html",
        "children": [{
                "title": "Classes",
                "key": "HCView",
                "href": "HCView.html"
                    }
]
            }
]
